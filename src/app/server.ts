import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Item, UsedCar, UsedCarPhoto } from './home/home.component';

@Injectable()
export class ServerControl {
  constructor(private http: HttpClient) {}

  private async request(method: string, url: string, data?: any) {
    const result = this.http.request(method, url, {
      body: data,
      responseType: 'json',
      observe: 'body',
      headers: new HttpHeaders().set('Content-Type', 'application/json'),
    });
    return new Promise((resolve) => {
      result.subscribe(resolve);
    });
  }

  getItems() {
    return this.request('GET', `${environment.serverUrl}/items`);
  }

  getItem(itemid: number) {
    return this.request('GET', `${environment.serverUrl}/items/singleitem/${itemid}`);
  }

  searchItemDB(searchKey: string, searchCategory: string) {
    return this.request(
      'GET',
      `${environment.serverUrl}/items/search/${searchKey}/${searchCategory}`
    );
  }

  createItemDB(item: Item) {
    return this.request('POST', `${environment.serverUrl}/items/createitem`, item);
  }

  updateItemDB(item: Item) {
    return this.request(
      'PUT',
      `${environment.serverUrl}/items/update/${item.id}`,
      item
    );
  }

  deleteItemDB(item: Item) {
    return this.request('DELETE', `${environment.serverUrl}/items/delete/${item.id}`);
  }

  saveUsedCar(usedCarData: UsedCar) {
    return this.request(
      'POST',
      `${environment.serverUrl}/usedcars/createusedcar`,
      usedCarData
    );
  }

  async saveUsedCarPhoto(usedCarPhoto: UsedCarPhoto) {
    return this.request(
      'POST',
      `${environment.serverUrl}/usedcarphotos/createusedcarphoto`,
      usedCarPhoto
    );
  }

  getUsedCars() {
    return this.request('GET', `${environment.serverUrl}/usedcars`);
  }

  getUsedCarsPhotos(carid?: string) {
    return this.request(
      'GET',
      `${environment.serverUrl}/usedcarphotos/listusedcarphotos/${carid}`
    );
  }

  deleteUsedCar(carid: any) {
    return this.request('DELETE', `${environment.serverUrl}/usedcars/deleteusedcar/${carid}`);
  }
}
