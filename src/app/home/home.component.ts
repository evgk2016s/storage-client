import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MdbModalRef, MdbModalService } from 'mdb-angular-ui-kit/modal';
import Swal from 'sweetalert2';
import { EdititemComponent } from '../edititem/edititem.component';
import { ServerControl } from '../server';
import { AwsS3Control } from '../uploadtos3';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  title = 'Storage Manager';

  @ViewChild('searchBar') searchBar!: ElementRef;

  modalRef: MdbModalRef<EdititemComponent> | null = null;

  newItem: Item = {
    id: 0,
    brand: '',
    model: '',
    name: '',
    frame_number: '',
    availability: 0,
    location: '',
    price: 0,
    photo_url: '',
    is_visible: false,
  };

  searchCategories = {
    nautral: 'Αναζήτηση κατηγοριών',
    active: 'Αναζήτηση κατηγοριών',
    option_one: 'Brand',
    option_two: 'Model',
    option_three: 'Frame Number',
  };

  itemsArray: Array<Item>;
  storageOpen: boolean = false;

  isFiltered: boolean = false;

  previousList: number = 0;
  currentList: number = 10;
  pages: Array<number> = [];

  image_url: string = '';

  constructor(
    private modalService: MdbModalService,
    private serverCtrl: ServerControl,
    private router: Router,
    private awsControl: AwsS3Control,
  ) {
    this.itemsArray = new Array<Item>();
  }

  ngOnInit(): void {
    this.openStorage();
  }

  openModalEdit(item: Item) {
    this.modalRef = this.modalService.open(EdititemComponent, {
      data: {
        id: item.id,
        brand: item.brand,
        model: item.model,
        name: item.name,
        frame_number: item.frame_number,
        quantity: item.availability,
        location: item.location,
        price: item.price,
        photo_url: item.photo_url,
        is_visible: !item.is_visible,
      },
    });
  }

  changeSearchCategory(opt: string) {
    switch (opt) {
      case 'option_one': {
        this.searchCategories.active = 'Brand';
        break;
      }
      case 'option_two': {
        this.searchCategories.active = 'Model';
        break;
      }
      case 'option_three': {
        this.searchCategories.active = 'Frame Number';
        break;
      }
    }
  }

  saveItem() {
    const brand = <HTMLInputElement>document.getElementById('newitembrand');
    const model = <HTMLInputElement>document.getElementById('newitemmodel');
    const name = <HTMLInputElement>document.getElementById('newitemname');
    const frame_number = <HTMLInputElement>document.getElementById('newitemfn');
    const availability = <HTMLInputElement>(
      document.getElementById('newitemquantity')
    );
    const location = <HTMLInputElement>(
      document.getElementById('newitemlocation')
    );
    const price = <HTMLInputElement>document.getElementById('newitemprice');
    const isVisible = <HTMLInputElement>document.getElementById('newitemhidden');
    this.newItem.brand = brand.value;
    this.newItem.model = model.value;
    this.newItem.name = name.value;
    this.newItem.frame_number = frame_number.value;
    this.newItem.availability = parseInt(availability.value, 10);
    this.newItem.location = location.value;
    this.newItem.price = parseFloat(price.value);
    this.newItem.photo_url = this.image_url;
    this.newItem.is_visible = !isVisible.checked;
    this.addItem();
  }

  clearFilters(): void {
    this.storageOpen = false;
    this.openStorage();
    this.isFiltered = false;
    this.searchBar.nativeElement.value = '';
  }

  addItem() {
    if (
      this.newItem.frame_number !== null &&
      this.newItem.frame_number.length > 0
    ) {
      this.serverCtrl.createItemDB(this.newItem).then((response: any) => {
        if (response.status === 'ok') {
          Swal.fire('Item successfully added', '', 'success');
          this.storageOpen = false;
          this.openStorage();
          const dismissBtn = <HTMLButtonElement>(
            document.getElementById('dismiss-modal')
          );
          dismissBtn.click();
        } else {
          Swal.fire('Something went wrong', '', 'error');
        }
      });
    }
  }

  print(item: Item) {
    this.router.navigate(['/print'], {
      queryParams: { itemid: item.id },
    });
  }

  deleteItem(item: Item) {
    Swal.fire({
      title:
        'Είσαι σίγουρος ότι θες να διαγράψεις το ' +
        item.name +
        ' με id ' +
        item.id +
        ' ?',
      text: 'Δεν θα μπορέσεις να το ανακτήσεις!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Διαγραφή!',
      cancelButtonText: 'Όχι, κράτησε το',
    }).then((result) => {
      if (result.value) {
        this.serverCtrl.deleteItemDB(item).then((response: any) => {
          if (response.status === 'ok') {
            Swal.fire(
              'Επυτυχής!',
              'Το αντικείμενο διαγράφηκε με επυτυχία.',
              'success'
            );
            // TODO delete item photo from s3 bucket
            this.awsControl.deleteFile(item.photo_url);
            this.storageOpen = false;
            this.openStorage();
          } else {
            Swal.fire('Κάτι πήγε λάθος', 'Ενημέρωσε την υποστήρηξη.', 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Ακύρωση', '', 'error');
      }
    });
  }

  searchItems() {
    const searchKey = this.searchBar.nativeElement.value;

    if (
      searchKey.length > 0 &&
      this.searchCategories.active !== 'Search Categories'
    ) {
      this.serverCtrl
        .searchItemDB(searchKey, this.searchCategories.active)
        .then((response: any) => {
          if (response.length > 0) {
            this.itemsArray = new Array<Item>();
            for (let i = 0; i < response.length; i++) {
              const item: Item = {
                id: response[i].id,
                brand: response[i].brand || 'Χωρίς Μάρκα',
                model: response[i].model || 'Χωρίς Μοντέλο',
                name: response[i].name || 'Χωρίς Όνομα',
                frame_number:
                  response[i].frame_number || 'Χωρίς Αριθμό Πλαισίου',
                availability: response[i].availability || 0,
                location: response[i].location || 'Χωρίς τοποθεσία',
                price: response[i].price || 'Χωρίς Τιμή',
                photo_url: response[i].photo_url || undefined,
                is_visible: response[i].is_visible,
              };
              this.itemsArray.push(item);
            }
            this.isFiltered = true;
          } else {
            Swal.fire('Δεν βρέθηκαν αποτελέσματα', '', 'error');
          }
        })
        .finally(() => {
          this.pages = [];
          const pages = this.itemsArray.length / 5;
          for (let i = 0; i < pages; i++) {
            this.pages.push(i);
          }
        });
    } else {
      Swal.fire('Δεν βρέθηκε λέξη κλειδί ή κατηγορία', '', 'error');
    }
  }

  openStorage() {
    if (!this.storageOpen) {
      this.itemsArray = new Array<Item>();
      this.storageOpen = true;

      // populate storage list
      this.serverCtrl
        .getItems()
        .then((response: any) => {
          for (let i = 0; i < response.length; i++) {
            const item: Item = {
              id: response[i].id,
              brand: response[i].brand || 'Χωρίς Μάρκα',
              model: response[i].model || 'Χωρίς Μοντέλο',
              name: response[i].name || 'Χωρίς Όνομα',
              frame_number: response[i].frame_number || 'Χωρίς Αριθμό Πλαισίου',
              availability: response[i].availability || 0,
              location: response[i].location || 'Χωρίς τοποθεσία',
              price: response[i].price || 'Χωρίς Τιμή',
              photo_url: response[i].photo_url || undefined,
              is_visible: response[i].is_visible,
            };
            this.itemsArray.push(item);
          }
        })
        .finally(() => {
          this.pages = [];
          const pages = this.itemsArray.length / 5;
          for (let i = 0; i < pages; i++) {
            this.pages.push(i);
          }
        });
    } else {
      this.storageOpen = false;
      // clear items list
      this.itemsArray = new Array<Item>();
    }
  }

  handlePage(event: any) {
    const pageIndex = event.pageIndex;
    const previousPageIndex = event.previousPageIndex;
    console.log(`PI : ${pageIndex} - PPI: ${previousPageIndex}`);
    this.previousList = pageIndex * 10;
    this.currentList = this.previousList + 10;
  }

  async onFileChange(event: any) {
    const file = event.target.files[0];
    this.image_url = await this.awsControl.useUpload(file);
    if (this.image_url) {
      Swal.fire('success', 'Photo uploaded success');
    }
  }

  addUsedCar() {
    this.router.navigate(['addusedcar']);
  }

  listUsedCars() {
    this.router.navigate(['listusedcars']);
  }
}

export interface Item {
  id: number;
  brand: string;
  model: string;
  name: string;
  frame_number: String;
  availability: number;
  location: string;
  price: number;
  photo_url: string;
  is_visible: boolean;
}

export interface UsedCar {
  id?: number;
  brand: String;
  model: String;
  price: Number;
  category: String;
  date: String;
  kms: Number;
  fuel: String;
  cc: Number;
  hp: Number;
  sasman: String;
  color: String;
  doors: Number;
  seats: Number;
  description: string;
  firstPhoto?: string;
}

export interface UsedCarPhoto {
  carid: number;
  photourl: String;
}
