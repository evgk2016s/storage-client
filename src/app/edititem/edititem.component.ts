import { Component, OnInit } from '@angular/core';
import { MdbModalRef } from 'mdb-angular-ui-kit/modal';
import Swal from 'sweetalert2';
import { Item } from '../home/home.component';
import { ServerControl } from '../server';

@Component({
  selector: 'app-edititem',
  templateUrl: './edititem.component.html',
  styleUrls: ['./edititem.component.css'],
})
export class EdititemComponent implements OnInit {
  id: number | null = null;
  brand: string | null = null;
  model: string | null = null;
  name: string | null = null;
  frame_number: string | null = null;
  quantity: number | null = null;
  location: string | null = null;
  price: number | null = null;
  photo_url: string | null = null;
  is_visible: boolean | null = null;

  constructor(
    public modalRef: MdbModalRef<EdititemComponent>,
    private serverCtrl: ServerControl
  ) {}

  editItem: Item = {
    id: 0,
    brand: '',
    model: '',
    name: '',
    frame_number: '',
    availability: 0,
    location: '',
    price: 0,
    photo_url: '',
    is_visible: false,
  };

  ngOnInit(): void {}

  saveChanges() {
    const id = this.id;
    const brand = <HTMLInputElement>document.getElementById('edititembrand');
    const model = <HTMLInputElement>document.getElementById('edititemmodel');
    const name = <HTMLInputElement>document.getElementById('edititemname');
    const frame_number = <HTMLInputElement>(
      document.getElementById('edititemfn')
    );
    const availability = <HTMLInputElement>(
      document.getElementById('edititemquantity')
    );
    const location = <HTMLInputElement>(
      document.getElementById('edititemlocation')
    );
    const price = <HTMLInputElement>document.getElementById('edititemprice');
    const isVisible = <HTMLInputElement>(
      document.getElementById('edititemhidden')
    );
    this.editItem.id = id!;
    this.editItem.brand = brand.value;
    this.editItem.model = model.value;
    this.editItem.name = name.value;
    this.editItem.frame_number = frame_number.value;
    this.editItem.availability = parseInt(availability.value, 10);
    this.editItem.location = location.value;
    this.editItem.price = parseFloat(price.value);
    this.editItem.is_visible = !isVisible.checked;

    this.serverCtrl.updateItemDB(this.editItem).then((response: any) => {
      if (response.status === 'ok') {
        Swal.fire('Επιτυχής Επεξεργασία', '', 'success');
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      }
    });

    this.modalRef.close();
  }
}
