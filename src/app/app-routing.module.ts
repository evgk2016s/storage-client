import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddusedcarComponent } from './addusedcar/addusedcar.component';
import { HomeComponent } from './home/home.component';
import { ListusedcarsComponent } from './listusedcars/listusedcars.component';
import { PrintPageComponent } from './print-page/print-page.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'print', component: PrintPageComponent },
  { path: 'addusedcar', component: AddusedcarComponent },
  { path: 'listusedcars', component: ListusedcarsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
