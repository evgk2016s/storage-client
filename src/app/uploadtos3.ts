import * as AWS from 'aws-sdk';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2';

@Injectable()
export class AwsS3Control {
  constructor(private http: HttpClient) {}

  uploadFile(file: File): Promise<string> {
    const s3 = new AWS.S3({
      accessKeyId: 'SOME ACCESS KEY ID',
      secretAccessKey: 'SOME SECRET ACCESS KEY',
      region: 'SERVER LOCATION',
    });

    const params = {
      Bucket: 'bucketName',
      Key: file.name,
      Body: file,
    };

    return new Promise((resolve, reject) => {
      s3.upload(params, (err: any, data: any) => {
        if (err) {
          reject(err);
        } else {
          resolve(data.Location);
        }
      });
    });
  }

  deleteFile(photoUrl: string) {
    const s3ObjectKey = photoUrl.substring(photoUrl.indexOf('.com/') + 5);
    console.log('deleting:' + s3ObjectKey);
    const s3 = new AWS.S3({
      accessKeyId: 'SOME ACCESS KEY ID',
      secretAccessKey: 'SOME SECRET ACCESS KEY',
      region: 'SERVER LOCATION',
    });
    const params = {
      Bucket: 'bucketName',
      Key: s3ObjectKey,
    };

    s3.deleteObject(params, function (err, data) {
      if (err) {
        Swal.fire('error', 'something went wrong deleting photo from object');
      } else {
        console.log(data);
      }
    });
  }

  async useUpload(myFile: File) {
    try {
      const location = await this.uploadFile(myFile);
      return location;
    } catch (err) {
      console.log(err);
    }
    return '0';
  }
}
