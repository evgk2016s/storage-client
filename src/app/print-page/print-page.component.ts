import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from '../home/home.component';
import { ServerControl } from '../server';

@Component({
  selector: 'app-print-page',
  templateUrl: './print-page.component.html',
  styleUrls: ['./print-page.component.css'],
})
export class PrintPageComponent implements OnInit {
  printItem: Item = {
    id: 0,
    brand: '',
    model: '',
    name: '',
    frame_number: '',
    availability: 0,
    location: '',
    price: 0,
    photo_url: '',
    is_visible: false,
  };

  constructor(
    private route: ActivatedRoute,
    private serverCtrl: ServerControl
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      const id = params['itemid'];
      this.serverCtrl.getItem(id).then((item: any) => {
        this.printItem.id = item[0].id;
        this.printItem.brand = item[0].brand;
        this.printItem.model = item[0].model;
        this.printItem.name = item[0].name;
        this.printItem.frame_number = item[0].frame_number;
        this.printItem.availability = item[0].availability;
        this.printItem.location = item[0].location;
        this.printItem.price = item[0].price;
      });
    });
  }

  printPage() {
    window.print();
  }
}
