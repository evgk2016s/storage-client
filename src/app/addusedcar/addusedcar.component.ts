import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UsedCar, UsedCarPhoto } from '../home/home.component';
import { ServerControl } from '../server';
import { AwsS3Control } from '../uploadtos3';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-addusedcar',
  templateUrl: './addusedcar.component.html',
  styleUrls: ['./addusedcar.component.css'],
})
export class AddusedcarComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private serverControl: ServerControl,
    private awsControl: AwsS3Control
  ) {}

  images: any = [];
  finalImages: any;

  usedCar = this.formBuilder.group({
    brand: '',
    model: '',
    price: '',
    category: '',
    date: '',
    kms: '',
    fuel: '',
    cc: '',
    hp: '',
    sasman: '',
    color: '',
    doors: '',
    seats: '',
    description: '',
  });

  ngOnInit(): void {
    this.finalImages = new Array<String>();
  }

  onSubmit(): void {
    // Process data here
    console.warn('Your used car has been submitted', this.usedCar.value);
    const usedCarData: UsedCar = {
      brand: this.usedCar.value.brand,
      model: this.usedCar.value.model,
      price: this.usedCar.value.price,
      category: this.usedCar.value.category,
      date: this.usedCar.value.date,
      kms: this.usedCar.value.kms,
      fuel: this.usedCar.value.fuel,
      cc: this.usedCar.value.cc,
      hp: this.usedCar.value.hp,
      sasman: this.usedCar.value.sasman,
      color: this.usedCar.value.color,
      doors: this.usedCar.value.doors,
      seats: this.usedCar.value.seats,
      description: this.usedCar.value.description,
    };
    this.serverControl.saveUsedCar(usedCarData).then((response: any) => {
      const carId = response.id;
      if (this.finalImages !== undefined) {
        for (let j = 0; j < this.finalImages.length; j++) {
          const image: UsedCarPhoto = {
            carid: carId,
            photourl: this.finalImages[j],
          };
          this.serverControl.saveUsedCarPhoto(image).then((result) => {
            // window.location.reload();
            console.log('uploaded' + result);
          });
        }
      }
    });
  }

  async onFileChange(event: any) {
    Swal.fire('warning', 'please wait while we upload your photos');
    for (let i = 0; i < event.target.files.length; i++) {
      this.finalImages.push(
        await this.awsControl.useUpload(event.target.files[i])
      );
    }
    Swal.fire('success', 'photos uploaded success');
  }
}
