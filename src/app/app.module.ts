import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { EdititemComponent } from './edititem/edititem.component';
import { MdbModalModule, MdbModalService } from 'mdb-angular-ui-kit/modal';
import { ServerControl } from './server';
import { PrintPageComponent } from './print-page/print-page.component';
import { AddusedcarComponent } from './addusedcar/addusedcar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatGridListModule } from '@angular/material/grid-list';
import { ListusedcarsComponent } from './listusedcars/listusedcars.component';
import { AwsS3Control } from './uploadtos3';

@NgModule({
  declarations: [AppComponent, HomeComponent, EdititemComponent, PrintPageComponent, AddusedcarComponent, ListusedcarsComponent],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule,
    MdbModalModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatGridListModule
  ],
  providers: [MdbModalService, ServerControl, AwsS3Control],
  bootstrap: [AppComponent],
})
export class AppModule {}
