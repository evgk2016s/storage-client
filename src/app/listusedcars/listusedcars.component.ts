import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { UsedCar } from '../home/home.component';
import { ServerControl } from '../server';
import { AwsS3Control } from '../uploadtos3';

@Component({
  selector: 'app-listusedcars',
  templateUrl: './listusedcars.component.html',
  styleUrls: ['./listusedcars.component.css'],
})
export class ListusedcarsComponent implements OnInit {
  constructor(private serverCtrl: ServerControl, private awsControl: AwsS3Control) {}

  usedCars?: Array<UsedCar>;

  ngOnInit(): void {
    this.usedCars = new Array<UsedCar>();

    this.serverCtrl.getUsedCars().then((response: any) => {
      for (let i = 0; i < response.length; i++) {
        const usedCar: UsedCar = {
          id: response[i].id,
          brand: response[i].brand,
          model: response[i].model,
          price: response[i].price,
          category: response[i].category,
          date: response[i].date,
          kms: response[i].kms,
          fuel: response[i].fuel,
          cc: response[i].cc,
          hp: response[i].hp,
          sasman: response[i].sasman,
          color: response[i].color,
          doors: response[i].doors,
          seats: response[i].seats,
          description: response[i].description,
        };
        this.serverCtrl
          .getUsedCarsPhotos(response[i].id)
          .then((photoRes: any) => {
            usedCar.firstPhoto = photoRes[0]?.photourl;
            this.usedCars?.push(usedCar);
          });
      }
    });
  }

  deleteUsedCar(car: UsedCar) {
    Swal.fire({
      title:
        'Είσαι σίγουρος ότι θες να διαγράψεις το ' +
        car.brand +
        ' με id ' +
        car.model +
        ' ?',
      text: 'Δεν θα μπορέσεις να το ανακτήσεις!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Διαγραφή!',
      cancelButtonText: 'Όχι, κράτησε το',
    }).then((result) => {
      if (result.value) {
        this.serverCtrl
          .getUsedCarsPhotos(car.id?.toString())
          .then((photos: any) => {
            for (let i = 0; i < photos.length; i++) {
              const url = photos[i].photourl;
              this.awsControl.deleteFile(url);
            }
          });
        this.serverCtrl.deleteUsedCar(car.id).then((response: any) => {
          if (response.status === 'ok') {
            Swal.fire(
              'Επυτυχής!',
              'Το αντικείμενο διαγράφηκε με επυτυχία.',
              'success'
            );
            // window.location.reload();
          } else {
            Swal.fire('Κάτι πήγε λάθος', 'Ενημέρωσε την υποστήρηξη.', 'error');
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire('Ακύρωση', '', 'error');
      }
    });
  }
}
